<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'NoteController@list');
Route::get('/dodaj-notatke', function () {
    return view('add_note');
});
Route::post('/dodaj-notatke', 'NoteController@create');
