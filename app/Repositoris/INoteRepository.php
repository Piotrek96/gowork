<?php
/**
 * Created by PhpStorm.
 * User: piotr
 * Date: 04.02.18
 * Time: 16:44
 */

namespace App\Repositoris;


use App\Note;

interface INoteRepository
{
    public function create(Note $note);

    public function findAll();

}