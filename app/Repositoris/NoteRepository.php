<?php
/**
 * Created by PhpStorm.
 * User: piotr
 * Date: 04.02.18
 * Time: 16:48
 */

namespace App\Repositoris;


use App\Note;
use App\NoteLink;
use App\NoteTodo;

class NoteRepository implements INoteRepository
{

    public function create(Note $note)
    {
        return $note->save();
    }

    public function createWithLink(Note $note, NoteLink $noteLink)
    {
        \DB::transaction(function () use ($note, $noteLink) {
            $note->save();
            $note->link()->save($noteLink);
        });
    }

    public function createWithTodo(Note $note, NoteTodo ...$todos)
    {
        \DB::transaction(function () use ($note, $todos) {
            $note->save();
            foreach ($todos as $todo) {
                $note->todo()->save($todo);
            }
        });
    }

    public function findAll()
    {
        return Note::all();
    }
}