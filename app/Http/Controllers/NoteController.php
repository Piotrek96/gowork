<?php

namespace App\Http\Controllers;

use App\Note;
use App\NoteLink;
use App\NoteTodo;
use App\Repositoris\NoteRepository;
use Illuminate\Http\Request;

class NoteController extends Controller
{
    /**
     * @var NoteRepository $noteRepository
     */
    private $noteRepository;

    /**
     * NoteController constructor.
     * @param NoteRepository $noteRepository
     */
    public function __construct(NoteRepository $noteRepository)
    {
        $this->noteRepository = $noteRepository;
    }

    public function list()
    {
        $noteList = Note::with(['link', 'todo'])->get();
        return view('note_list')->with('notes', $noteList);

    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(Request $request)
    {
        $note = new Note();
        $note->type = $request->noteType;
        $note->title = $request->noteTitle;
        $methodName = $this->getMethodName($note);
        $this->{$methodName}($request, $note);
        return redirect('/');
    }

    /**
     * @param Request $request
     * @param $note
     */
    private function createTodo(Request $request, $note): void
    {
        $todos = [];
        foreach ($request->note['todo'] as $todo) {
            if ($todo !== null) {
                $noteTodo = new NoteTodo();
                $noteTodo->task = $todo;
                $todos[] = $noteTodo;
            }
        }
        $this->noteRepository->createWithTodo($note, ...$todos);
    }

    /**
     * @param Request $request
     * @param $note
     */
    private function createLink(Request $request, $note): void
    {
        $noteLink = new NoteLink();
        $noteLink->link = $request->note['link'];
        $this->noteRepository->createWithLink($note, $noteLink);
    }

    /**
     * @param $note
     * @return string
     */
    private function getMethodName(Note $note): string
    {
        $methodName = 'create' . strtoupper($note->type);
        if (!method_exists($this, $methodName)) {
            throw new \InvalidArgumentException();
        }
        return $methodName;
    }

}
