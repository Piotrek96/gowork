<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    public function link(){
        return $this->hasOne('App\NoteLink');
    }
    public function todo(){
        return $this->hasMany('App\NoteTodo');
    }
}
