Set data in.env file for:   
    DB_CONNECTION  
    DB_HOST  
    DB_PORT  
    DB_DATABASE  
    DB_USERNAME  
    DB_PASSWORD  
install composer dependencies  
php artisan migrate  
php artisan serve  
  
Pliki do zobaczenia:   
    App\Http\Controllers\NoteController  
    App\Http\RepositoriesNoteRepository  
    App\Note*  
    resources/views/*  
  