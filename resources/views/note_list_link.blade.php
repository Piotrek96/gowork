<div class="card" style="width: 18rem;">
    <div class="card-body">
        <h5 class="card-title">{{ $note->title }}</h5>
        <a href="{{$note->link->link}}" class="card-link">{{$note->link->link}}</a>
    </div>
</div>