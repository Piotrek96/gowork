<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->

    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <a href="/">Powrót</a>
    <form action="dodaj-notatke" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="exampleFormControlInput1">Tytuł notatki</label>
            <input type="text" class="form-control" name="noteTitle" placeholder="Tytuł" required>
        </div>
        <div class="form-group">
            <label for="exampleFormControlSelect1">Typ notatki</label>
            <select class="form-control" name="noteType" id="noteTypeSelection" required>
                <option selected value="link">Link</option>
                <option value="todo">TODO</option>
            </select>
        </div>
        <div id="link" class="d-none showable">
            <div class="form-group">
                <label for="exampleFormControlInput1">Link</label>
                <input type="text" class="form-control" name="note[link]" placeholder="link do źródła">
            </div>
        </div>
        <div id="todo" class="d-none showable">
            <div class="form-group">
                <label for="exampleFormControlInput1">Lista TODO</label>
                <input type="text" class="form-control" name="note[todo][]" placeholder="todo#1">
                <input type="text" class="form-control" name="note[todo][]" placeholder="todo#2">
                <input type="text" class="form-control" name="note[todo][]" placeholder="todo#3">
                <input type="text" class="form-control" name="note[todo][]" placeholder="todo#4">
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Dodaj</button>
    </form>

    <script>
        function setInputVisibility(value) {
            function setRequired() {
                Array.from(document.querySelectorAll('.showable input')).forEach((element) => {
                    element.required = false;
                });
                Array.from(document.querySelectorAll(`#${value}.showable input`)).forEach((element) => {
                    element.required = true;
                });
            }
            function setVisibility() {
                Array.from(document.getElementsByClassName('showable')).forEach((element) => {
                    element.classList.add('d-none')
                });
                document.getElementById(value).classList.remove('d-none');
            }
            setRequired();
            setVisibility();
        }
        setInputVisibility(document.getElementById('noteTypeSelection').value);
        document.getElementById("noteTypeSelection").addEventListener('change', function (e) {
            setInputVisibility(e.target.value);
        });
    </script>
</div>
</body>
</html>


