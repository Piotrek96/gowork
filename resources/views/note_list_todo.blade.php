<div class="card" style="width: 18rem;">
    <div class="card-body">
        <h5 class="card-title">{{ $note->title }}</h5>
        <ul class="list-group list-group-flush">
            @foreach($note->todo as $todo)
                <li class="list-group-item">{{$todo->task}}</li>
            @endforeach
        </ul>
    </div>
</div>